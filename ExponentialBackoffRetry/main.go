package main

import (
	"fmt"
	"math/rand"
	"net/http"
	"time"
)

func exponentialBackOffRetry(f func() (*http.Response, error), w []int, maxJitter, minJitter int) (*http.Response, error) {

	var response *http.Response
	var finalError error

	for i := 0; i < len(w); i++ {

		resp, err := f()

		response = resp
		finalError = err

		if err == nil {
			return resp, err
		}

		//TODO: Separate out retryable errors from non-retryable errors.

		// create Jitter
		jitter := rand.Intn(maxJitter-minJitter) + minJitter
		jitterDuration := time.Duration(jitter) * time.Millisecond

		// read wait times.
		backoffInterval := time.Duration(w[i]) * time.Second

		start := time.Now()

		// add jitter to wait times and kick off sleep.
		time.Sleep(jitterDuration + backoffInterval)

		elapsed := time.Since(start)
		fmt.Printf("Total runtime:%v, Backoff Duration: %v, JitterDuration: %v, Jitter: %v\n", elapsed, backoffInterval, jitterDuration, jitter)

	}
	return response, finalError
}

func main() {

	fetchContent := func() (*http.Response, error) {
		resp, err := http.Get("http://example11111.com/")
		return resp, err
	}

	// Dictates the backoff time intervals.
	waitTimes := []int{0, 1, 2, 5, 10}

	// Jitter spectrum in seconds.
	maxJitter := 90
	minJitter := 60

	resp, err := exponentialBackOffRetry(fetchContent, waitTimes, maxJitter, minJitter)

	fmt.Println(resp, err)

}
