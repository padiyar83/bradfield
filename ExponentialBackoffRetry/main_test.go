package main

import (
	"net/http"
	"reflect"
	"testing"
)

func Test_exponentialBackOffRetry(t *testing.T) {
	type args struct {
		f         func() (*http.Response, error)
		w         []int
		maxJitter int
		minJitter int
	}
	tests := []struct {
		name    string
		args    args
		want    *http.Response
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := exponentialBackOffRetry(tt.args.f, tt.args.w, tt.args.maxJitter, tt.args.minJitter)
			if (err != nil) != tt.wantErr {
				t.Errorf("exponentialBackOffRetry() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("exponentialBackOffRetry() = %v, want %v", got, tt.want)
			}
		})
	}
}
