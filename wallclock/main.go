package main

import (
	"encoding/json"
	"log"
	"net/http"
	"time"
)

// ResponseTime ..
type ResponseTime struct {
	Timezone string `json:"Timezone"`
	Time     string `json:"Time"`
}

// Takes in a IANA location and gives out the time right now in that location.
func timeInLocationNow(timezone string) (string, error) {
	t := time.Now()

	loc, err := time.LoadLocation(timezone)

	if err == nil {
		return t.In(loc).Format("15:04:05"), err
	}
	return t.Format("15:04:05"), err
}

func handler(w http.ResponseWriter, r *http.Request) {
	t, _ := timeInLocationNow(r.URL.Path[1:])

	responseTime := ResponseTime{r.URL.Path[1:], t}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseTime)

}

func main() {
	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

// Output

// vinny@Vinnys-MacBook-Pro  ~/Projects/languages/go/src/bitbucket.com/padiyar83/wallclock  curl -i localhost:8080/America/New_York
// HTTP/1.1 200 OK
// Content-Type: application/json
// Date: Mon, 27 Apr 2020 05:05:03 GMT
// Content-Length: 50

// {"Timezone":"America/New_York","Time":"01:05:03"}
