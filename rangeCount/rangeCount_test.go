// Consider the problem of counting occurrences of a given value x within a sorted array a.
// For example, if x=5 and a=[1,1,2,4,5,5,7,9], then the count is 2.
//				i. Write a function that solves this problem by performing a linear scan.
// 				ii. Next, write a function that solves this problem by performing two binary searches.
// 				iii. Finally, benchmark your two functions for random sorted arrays of size 10, 100, ..., up to 10,000,000. How does performance compare between the two functions?

package main

import (
	"fmt"
	"testing"
	"time"
)

func ProduceRandomSortedList(size int) []int { 
	
	arr := []int{}
	for i := 0; i < size; i++ {
		arr = append(arr, rand.Intn())
	}
	sort.Intn(arr)
	return arr
}

func TestSearch(t *testing.T) {
	type args struct {
		nums []int
		key  int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{"test1", args{[]int{1,1,2,4,5,5,7,9}, 5}, 2,},
		{"test2", args{[]int{1,1,2,2,3,3,4,4}, 1}, 2,},
		{"test3", args{[]int{1,1,2,2,3,3,4,4}, 3}, 2,},
		{"test4", args{[]int{1,1}, 1}, 2,},
		{"test5", args{[]int{4,4}, 1}, 0,},
		{"test6", args{[]int{4,3,3}, 3}, 2,},
		{"test7", args{[]int{4,2}, 2}, 1,},
		{"test8", args{[]int{1}, 1}, 1,},
		{"test9", args{[]int{}, 1}, 0,},
		{"test10", args{[]int{0, 0, 2, 2, 2, 3, 3, 5, 5, 5, 6, 7, 8, 10, 11, 11, 13, 15, 18, 18, 20, 21, 23, 24, 25, 26, 28, 28, 28, 29, 31, 31, 33, 33, 33, 36, 37, 37, 37, 38, 40, 40, 41, 41, 43, 43, 45, 46, 46, 47, 47, 47, 47, 47, 51, 52, 53, 53, 55, 56, 56, 56, 57, 58, 59, 59, 59, 61, 62, 63, 63, 63, 66, 66, 74, 76, 77, 78, 78, 81, 81, 83, 85, 87, 87, 87, 88, 88, 89, 89, 90, 90, 91, 94, 94, 94, 95, 96, 98, 99}, 56}, 3},
	}


	for _, tt := range tests {

		
		t.Run(tt.name, func(t *testing.T) {
			start := time.Now()
			if got := linearSearch(tt.args.nums, tt.args.key); got != tt.want {
				t.Errorf("linearSearch() = %v, want %v", got, tt.want)
			}
			elapsed := time.Since(start)
			fmt.Println("linearSearch", tt.name, elapsed)

			start = time.Now()

			
			if got := binarySearch(tt.args.nums, tt.args.key); got != tt.want {
				t.Errorf("binarySearch() = %v, want %v", got, tt.want)
			}
			elapsed = time.Since(start)
			fmt.Println("binarysearch", tt.name, elapsed)

		})
	}
}

