// Consider the problem of counting occurrences of a given value x within a sorted array a. 
// For example, if x=5 and a=[1,1,2,4,5,5,7,9], then the count is 2.
//				i. Write a function that solves this problem by performing a linear scan.
// 				ii. Next, write a function that solves this problem by performing two binary searches.
// 				iii. Finally, benchmark your two functions for random sorted arrays of size 10, 100, ..., up to 10,000,000. How does performance compare between the two functions?


package main

import (
	"fmt"
	"math/rand"
	"sort"
	"time"
)



func linearSearch(nums []int, key int ) int { 
	
	count := 0
	for _, i := range nums  { 
		if i == key { 
			 count ++
		}
	}
	return count
	
}

func binarySearch(nums []int, key int) int { 

	
	count := 0

	if len(nums) == 0 { 
		return 0
	}

	if len(nums) == 1 && nums[0] == key { 
		return 1
	}

	if len(nums) == 2 { 

		if nums[0] == key && nums[1] == key { 
			return 2
		} 
		if nums[0] == key || nums[1] == key { 
			return 1
		}
		return 0 
	}
	
	for len(nums) > 2 { 


		// Handles the case where the array length is greater than 2, but the key is in 0th postion.
		if nums[0] == key { 
			for i := 0; i < len(nums); i++ { 
				if nums[i] != key { 
					break
				}
				count ++
				 
			}
			break
		}

		// Handles the case where the array length is greater than 2, but key is in 1st position.
		if nums[1] == key { 
			for i := 1; i < len(nums); i++ { 
				if nums[i] != key { 
					break
				}
				count ++
			}
			break
		}

		
		// Handle all other cases.
		midpoint := len(nums)/2
	
		if key == nums[midpoint] { 
			for i := midpoint; i < len(nums); i++ { 
				if nums[i] != key { 
					break
				}
				count ++
			}
			break
		
			for i := midpoint; i < len(nums); i-- { 
				if nums[i] != key { 
					break
				}
				count ++ 
			}	
			break
		}
	


		if key < nums[midpoint] { 
			nums = nums[:midpoint]
		} else { 
			nums = nums[midpoint:]
		}
	}

	

	return count
	
	
}



func getRandomIntArr(size int) []int { 
	
	arr := []int{}
	for i := 1; i < size; i++ {
		arr = append(arr, rand.Intn(i))
	}
	sort.Ints(arr)
	return arr
}


func main() {

	
	fmt.Println("Time comparision for 100 integer list")
	start := time.Now()
	g := getRandomIntArr(100)
	linearSearch(g, 1000)
	elapsed := time.Since(start)
	fmt.Println("Linear search execution time", elapsed)

	start = time.Now()
	binarySearch(g, 1000)
	elapsed = time.Since(start)
	fmt.Println("Binary search execution time", elapsed)

	fmt.Println("-------------------------------")

	fmt.Println("Time comparision for 1000 integer list")
	g = getRandomIntArr(1000)
	start = time.Now()
	linearSearch(g, 10000)
	elapsed = time.Since(start)
	fmt.Println("Linear search execution time", elapsed)

	start = time.Now()
	binarySearch(g, 10000)
	elapsed = time.Since(start)
	fmt.Println("Binary search execution time", elapsed)

	fmt.Println("-------------------------------")


	fmt.Println("Time comparision for 10000 integer list")
	g = getRandomIntArr(10000)
	start = time.Now()
	linearSearch(g, 100000)
	elapsed = time.Since(start)
	fmt.Println("Linear search execution time", elapsed)

	start = time.Now()
	binarySearch(g, 100000)
	elapsed = time.Since(start)
	fmt.Println("Binary search execution time", elapsed)

	fmt.Println("-------------------------------")


	fmt.Println("Time comparision for 100000 integer list")
	g = getRandomIntArr(100000)
	start = time.Now()
	linearSearch(g, 1000000)
	elapsed = time.Since(start)
	fmt.Println("Linear search execution time", elapsed)

	start = time.Now()
	binarySearch(g, 1000000)
	elapsed = time.Since(start)
	fmt.Println("Binary search execution time", elapsed)

	fmt.Println("-------------------------------")


	fmt.Println("Time comparision for 1000000 integer list")
	g = getRandomIntArr(1000000)
	start = time.Now()
	linearSearch(g, 100000)
	elapsed = time.Since(start)
	fmt.Println("Linear search execution time", elapsed)

	start = time.Now()
	binarySearch(g, 100000)
	elapsed = time.Since(start)
	fmt.Println("Binary search execution time", elapsed)
	
	fmt.Println("-------------------------------")

	fmt.Println("Time comparision for 10000000 integer list")
	g = getRandomIntArr(10000000)
	start = time.Now()
	linearSearch(g, 100000000)
	elapsed = time.Since(start)
	fmt.Println("Linear search execution time", elapsed)

	start = time.Now()
	binarySearch(g, 100000000)
	elapsed = time.Since(start)
	fmt.Println("Binary search execution time", elapsed)

	fmt.Println("-------------------------------")


	fmt.Println("Time comparision for 100000000 integer list")
	g = getRandomIntArr(100000000)
	start = time.Now()
	linearSearch(g, 1000000000)
	elapsed = time.Since(start)
	fmt.Println("Linear search execution time", elapsed)

	start = time.Now()
	binarySearch(g, 1000000000)
	elapsed = time.Since(start)
	fmt.Println("Binary search execution time", elapsed)
}